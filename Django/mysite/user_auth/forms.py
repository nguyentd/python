from django import forms

class RegisterForm(forms.Form):
    username = forms.CharField(label="username", max_length=30)
    passwd = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField(label = "email")