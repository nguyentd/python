from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .forms import RegisterForm

def register(request):
    if request.method == 'POST':
        reponse = HttpResponse()
        reponse.write("<h1>Thanks </h1></br>")
        reponse.write("Your name: "+ request.POST['username']+ "</br>")
        reponse.write("Email: "+ request.POST['email']+ "</br>")
        return reponse
    registerForm = RegisterForm()
    return render(request,'user_auth/register.html',{'form':registerForm})
