from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from .forms import UpFile

def fileUpView(request):
    if request.method == 'POST':
        form = UpFile(request.POST,request.FILES)
        if form.is_valid():
            upload(request.FILES['file_up'])
            return HttpResponse("<h2>File uploade OK </h2>")
        else:
            return HttpResponse("<h2>File uploaed not OK </h2>")
    form = UpFile()
    return render(request,'file_up/up.html',{'form':form})

def upload(f):
    file_ = open(f.name,'wb+')
    for chunk in f.chunks():
        file_.write(chunk)