from django import forms

class UpFile(forms.Form):
    title = forms.CharField(max_length=30)
    file_up = forms.FileField()