from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from .models import Question, Choice
from django.template import loader
from django.urls import reverse
from django.views import generic

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'last_question_list'
    def get_queryset(self):
        return Question.objects.order_by('-pub_date')[:5]

# def index(request):
#     # reponse = HttpResponse()
#     # reponse.write('<b>Hello</b>')
#     # return reponse
#     last_question_list = Question.objects.order_by('-pub_date')[:5]
#     #output = ', '.join([q.question_text for q in last_question_list])
#     #return HttpResponse(output)
#     template = loader.get_template('polls/index.html')
#     context = {
#         'last_question_list': last_question_list,
#     }
#     return HttpResponse(template.render(context,request))

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/details.html'

# def detail(request,question_id):
#     try:
#         question = Question.objects.get(pk=question_id)
#     except Question.DoesNotExist:
#         raise Http404("Not exist")
    
#     contex = {
#         'question': question
#     }
        
#     return render(request,'polls/details.html',contex)
class ResultView(generic.DetailView):
    model = Question
    template_name = 'polls/result.html'
# def result(request,question_id):
#     #reponse = "You're looking at the results of question %s."
#     #return HttpResponse(reponse %question_id)
#     question = get_object_or_404(Question,pk = question_id)
#     return render(request,'polls/result.html',{'question':question})

def vote(request,question_id):
    #return HttpResponse("You're voting on question %s." % question_id)
    question = get_object_or_404(Question,pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except(KeyError,Choice.DoesNotExist):
        return render(request,'polls/details.html',{
            'question': question,
            "error_message": "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
    return HttpResponseRedirect(reverse('polls:result',args=(question.id,)))