
from tkinter import Tk, RIGHT,BOTH, RAISED

from tkinter.ttk import Frame, Button, Style


class Ex(Frame):
    def __init__(self,parent):
        Frame.__init__(self,parent)
        self.parent = parent
        self.initUI()

    def onClose(self):
        print("Close!")
        exit()

    def onOK(self):
        print("OK!")

    def initUI(self):
        self.parent.title("Button")
        self.style = Style()
        self.style.theme_use("default")

        #frame = Frame(relief=RAISED,borderwidth = 1)
        #frame.pack(fill = BOTH,expand=True)
        self.pack(fill = BOTH,expand=True)
        closeButton = Button(self, text="Close",command=self.onClose)
        closeButton.pack(side=RIGHT, padx=5, pady=5)
        okButton = Button(self, text="OK", command = self.onOK)
        okButton.pack(side=RIGHT)

root = Tk()
root.geometry("300x200+300+300")
app = Ex(root)
root.mainloop()